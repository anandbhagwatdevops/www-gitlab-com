title: Crédit Agricole
cover_image: '/images/blogimages/bgs-cover.jpg'
cover_title: |
  How Crédit Agricole transformed its global workflow with GitLab
cover_description: |
  Through user preference, Crédit Agricole adopted GitLab for SCM, robust integration management, and seamless automation.
twitter_image: '/images/opengraph/Case-Studies/case-study-bgs.png'

twitter_text: "Learn how @BritGeoSurvey accelerates innovative science research with @GitLab."

customer_logo: '/images/case_study_logos/bgs-logo-reverse.png'
customer_logo_css_class: brand-logo-tall
customer_industry: Financial
customer_location: New York, Singapore, London, Paris
customer_solution: GitLab Enterprise
customer_employees: 7,395 
customer_overview: |
  Worldwide banking institute, Crédit Agricole, adopted GitLab for SCM, but found it offered automation and improved efficiency.
customer_challenge: |
  Crédit Agricole was looking to move to a Git solution and wanted users to decide on the platform.

key_benefits:
  - |
    Improved workflow efficiency 
  - |
    Cross-company collaboration
  - |
    Ease of integration
  - |
    Faster time to delivery
  - |
    Integrated task management
  - |
    Kubernetes integration
  - |
    Provided automation 

customer_stats:
  - stat: 50-75%  
    label: Time to delivery saved
  - stat: 2,000      
    label: Organic users 
  - stat: <1 day   
    label: To create a new application 


customer_study_content:
  - title: the customer
    subtitle: French banking leader
    content:
      - |
        Crédit Agricole is a finance and investment bank that is ranked amongst the top 10 worldwide. Crédit Agricole is the world’s largest cooperative financial institution, known for its historical ties to farming. The primary functions include financial services for large companies, public sectors, investment, and real economy needs. The corporate identity of the financial institution is called CA-CIB, and there are teams in approximately 35 countries.

  - title: the challenge
    subtitle: Lacking worldwide workflow efficiency
    content:
      - |
        [Crédit Agricole](https://www.credit-agricole.com/en/) was using Subversion as its centralized tool. From an efficiency standpoint, and particularly for remote teams, Subversion was detrimental to performance, code retrieval, and the implementation and updating of processes. The tool was deployed in the Paris data center, which meant it had to travel over 8,000 miles to Singapore in order to reach developers. The developer experience was greatly impaired in terms of performance and flexibility.   
      - |
        On top of that, branch management wasn’t standardized, and elements that weren’t source code were being stored in repositories. “We primarily needed to manage our solutions globally. I think I’m right in saying that today, Subversion is an obsolete system. It’s practically no longer taught in schools. Everyone uses Git for everything,” said Nicolas Jautée, Software Factory and DevOps Expert. Crédit Agricole was concerned that by not using Git, they were falling behind industry standards. It was also difficult to work with clients who use Git -- essentially forcing clients into using SVN and according to Jautée, “they felt like they were going backwards.”
      - |
        There was a lack of native integration with the tooling systems, and also among the teams. The DevOps teams manage the platform element and create access in order for teams to then manage their own content, creating as many repositories as necessary for code storage without admin constraint. One of the reasons Crédit Agricole was looking to replace Subversion was for integration with autonomy. “The goal is to have teams that can be independent and responsible within their perimeters with our support and our expertise (if needed), while being able to go as far as possible on their own to be reactive within their businesses,” Jautée said. 
 
  - blockquote: In KPIs, we can add the very fast, wide distribution of users as well as automatic chains that we installed around them. Today, this is an important pillar for many IT processes. 
    attribution: Sedat Guclu 
    attribution_title: Head of Software Factory and Devops, Crédit Agricole 
   
  - title: the solution
    subtitle: GitLab spreads through word of mouth
    content:
      - |
        Crédit Agricole intended to move from Subversion and adopt a version of Git. Initially, users were pushing to switch to GitHub. The teams requested a proof of concept with GitHub and GitLab. “At that time, we looked at GitLab and noticed that it offered great functionalities, even better than GitHub. In terms of implementation, it was more relevant and easier to manage than the GitHub black box,” Jautée said. 
      - |
        About three years earlier, Crédit Agricole had done a forced migration to Subversion from ClearCase. They did not want to launch a force migration to GitLab for users and make the same mistake. “Consequently, for GitLab, we let word of mouth and user preference do the job. The fact is that the licenses we bought the first year were used within less than 9 months. I think we had to purchase more licenses even before the end of the first year even though we had not done any official communication on this tool; we only let viral adoption take its course.” Jautée said.
      - |
        Contrary to what is customary, GitLab was launched from the bottom up, promoted initially from the project teams. Infrastructure teams also helped by testing functionalities and documentation. The bottom-up building and viral distribution model resulted in a rapid adoption rate. “People who really wanted to use this tool knew why they liked it; we didn’t launch a migration campaign imposed by our top management. So far, we haven’t found it necessary to do so, since projects have been coming in naturally and users saw the benefits. This is to the point where today, we provide IT for IT, since that’s our job, but we quickly found ourselves in a situation where the tool is exceeding the initial perimeter we anticipated,” said Sedat Guclu, Head of Software Factory and Devops, Crédit Agricole.
      - |
        When teams were using SVN, they had to take care of all the administrative steps, adding users, maintaining logins, etc. The process was hard to manage and time consuming, causing lengthy delays for those using it. One priority was to free up developer time with a self-service solution. Consequently, GitLab provided significant cost savings that they hadn’t previously planned on. “The community-based solution, with unlimited and free use, enables us to create documentation and tutorials, make POCs, prototypes and inner source development, to which we can have everyone contribute without having to worry about the number of licenses required,” Guclu said. “So, we keep a community-based platform, and we have an ‘Enterprise’ platform hosting the business code and all the automation systems, whether from Infrastructure, CI-CD or other, to be able to cover both needs and better distribute the usage accordingly.” 
      
  - blockquote: Switching to GitLab made us go from several days to create a new application to just a few hours. 
    attribution: Sedat Guclu
    attribution_title: Head of Software Factory and Devops, Crédit Agricole
    
  - title: the results
    subtitle:  Exceeding business application expectations
    content:
      - |
        “GitLab is much more than a mere source manager. There is a whole ecosystem around it: integrated task management, integrated CI, etc., which is totally absent from Subversion. These were the primary aspects that motivated our initial approach,” Jautée added.
      - |
        A standout benefit of GitLab is the ability to integrate it into the existing environment. It is simpler for the teams to use a tool that can be installed and mastered quickly. The second benefit that stood out over GitHub is the superior functionality. The ability to not only manage code, but also delegate the long-term management of the content to perimeter managers.    
      - |
        The infrastructure teams now use GitLab for automation tasks, including machine building on a private cloud. Application release processes are also based in GitLab. “This isn’t exactly what we had foreseen. While our goal was to host the code for our business applications, other teams, by extension, saw additional benefits,” Guclu said. “For instance, our security teams use it to distribute guidelines in the form of Wikis to help teams with the detection and correction of vulnerabilities. This is a significant innovation compared to what we used to do.”
      - |
        Crédit Agricole now has GitLab users on the business side, including quantitative analysts who work on risk management processes and use the tool for IT tasks and scientific documentation. Some team members use Mermaid or LaTeX modules that integrate into GitLab to generate documentation for calculation model libraries. “These are more uses that we hadn’t predicted. The viral mode went well beyond what we originally imagined, since we started with a small number of users. In the first year we had 400 users and today we have 2,000 Enterprise users, and this number is increasing daily. The growth is now self-sustaining, and that has helped us enormously, both in terms of standardization and automation,” according to Guclu.  
      - |
       Onboarding has also been simplified and streamlined with GitLab. According to Guclu, “We might have needed 5 to 10 days to apply onboarding. Today, we can do it within a day. It only takes a few hours to create the space. If the person already has the authorizations, it’s almost instantaneous: We do it in minutes, then they manage the entire process and create their own repositories.”
      - |
        Developers now have the ability to perform migrations themselves with existing code. The primary benefit is that it allows them to make ‘blank’ migrations to test the retrieved code and adapt environments to be able to run in parallel with the application development. In the meantime, they can execute tests in GitLab while continuing deliveries, since a ‘hot fix’ isn’t necessary on the application. “For a migration, we went from almost a month with ClearCase to a few days that can be modulated according to the project priorities. It’s up to them to decide when they want to do it, how they test, and how they configure their factories at the same time, to continue delivering the existing applications while verifying that the work is done the same way in GitLab. That’s also a real comfort,” Guclu said.
      - |
        By the end of 2020, Crédit Agricole plans to completely remove Subversion from its workflow. In the three years since the adoption of GitLab, they went from zero users to 1500 without forcing it on teams. There is now a large automation chain, with over 200 runners, which means that they notonly adopted GitLab for code management, but also for automation. “There are people in datacenters who are also using GitLab for notebooks they develop with Anaconda or similar solutions, that never used this type of solution before. They didn’t see any advantage in managing their source code in this type of tool. That is actually what they told us,” Guclu said. “Today, we see that they are also asking for this type of solution, because it allows them to collaborate more easily with other members of their team.”
      - |


      - |
        ## Learn more about Self-Managed Ultimate
      - |
        [Why source code management matters](/stages-devops-lifecycle/source-code-management/)
      - |
        [The benefits of GitLab CI](/stages-devops-lifecycle/continuous-integration/)
      - |
        [Choose a plan that suits your needs](/pricing/)

